﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Filter;
using LogAnalyzerLib.Reader;
using PluginSystem.Messages;

namespace BasicFilters
{
    /// <summary> Alle Einträge, die nicht das Level des Übergebenen Eintrags haben
    /// </summary>
    class ExcludeLevelFilter : Filter
    {
        public ExcludeLevelFilter():base("Exclude Level Filter", "Shows all entries not from the given Entries Level",true)
        {
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            return "Level is not " + source.level;
        }


        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            message(PluginMessage.MessageLevel.Info, "Filter for Level not " + source.level);
            return logsfile.Where(l => l.level != source.level).ToList();
        }
    }
}
