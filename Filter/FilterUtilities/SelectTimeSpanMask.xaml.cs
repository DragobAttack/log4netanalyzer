﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilterUtilities
{
    /// <summary>
    /// Interaktionslogik für SelectTimeSpan.xaml
    /// </summary>
    public partial class SelectTimeSpanMask : Window
    {        
        private bool _applyValue;

        DateTimePicker startTimePicker;

        DateTimePicker endTimePicker;

        public DateTime StartTime
        {
            get
            {
                return getDateValue(dpDateStart.SelectedDate) + startTimePicker.Value.TimeOfDay;
            }
        }
        public DateTime EndTime
        {
            get
            {
                return getDateValue(dpDateEnd.SelectedDate) + endTimePicker.Value.TimeOfDay;
            }
        }

        public bool ApplyValue
        {
            get
            {
                return _applyValue;
            }
        }

        public SelectTimeSpanMask(DateTime minTime, DateTime maxTime)
        {
            InitializeComponent();

            startTimePicker = createDateTimePicker(gdTimeStart, minTime, maxTime, DateTimePickerFormat.Time);
            startTimePicker.MinDate = minTime;
            startTimePicker.MaxDate = maxTime;
            startTimePicker.Value = startTimePicker.MinDate;
            //startDatePicker = createDateTimePicker(gdDateStart, minTime, maxTime, DateTimePickerFormat.Short);
            dpDateStart.SelectedDate = minTime;

            endTimePicker = createDateTimePicker(gdTimeEnd, minTime, maxTime, DateTimePickerFormat.Time);
            endTimePicker.MinDate = minTime;
            endTimePicker.MaxDate = maxTime;
            endTimePicker.Value = endTimePicker.MaxDate;
            //endDatePicker = createDateTimePicker(gdDateEnd, minTime, maxTime, DateTimePickerFormat.Short);
            dpDateEnd.SelectedDate = maxTime;

            _applyValue = false;           
        }        

        private void btnApplyFilter_Click(object sender, RoutedEventArgs e)
        {
            _applyValue = true;
            this.Close();
        }

        private DateTimePicker createDateTimePicker(Grid target, DateTime min, DateTime max, DateTimePickerFormat format)
        {
            var host = new System.Windows.Forms.Integration.WindowsFormsHost();
            DateTimePicker picker = new DateTimePicker();

            picker.MinDate = min;
            picker.MaxDate = max;

            picker.Format = format;

            if (format == DateTimePickerFormat.Time)
                picker.ShowUpDown = true;

            host.Child = picker;
            target.Children.Add(host);

            return picker;
        }

        private DateTime getDateValue(DateTime? dt)
        {
            if (dt.HasValue)
            {
                return dt.Value;
            }
            else
                return System.DateTime.MinValue;
        }
    }
}
