﻿using LogAnalyzerLib.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LogAnalyzerLib.Reader;

namespace ExtendedFilters.SelectRegex
{
    /// <summary> Suche nach Einträgen, die einer Regex entsprechen
    /// </summary>
    public class RegexFilter:Filter
    {
        public RegexFilter():base("Regex Filter", "Use a Regular Expression on Log Messages",false)
        {
        }

        public override object configure(List<LogEntry> logsfile, out bool apply)
        {
            RegexFilterMask mask = new RegexFilterMask();
            mask.ShowDialog();
            if (!string.IsNullOrEmpty(mask.regex))
                apply = true;
            else
                apply = false;

            return mask.regex;
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            string des = additional.ToString();

            if (des.Length > 12)
                des = additional.ToString() + "...";

            return "Apply Regex " + des;
        }

        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            Regex reg = new Regex(additional.ToString());

            List<LogEntry> matching = new List<LogEntry>();

            foreach (var entry in logsfile)
            {
                if (reg.Match(entry.message).Success)
                    matching.Add(entry);
            }

            return matching;
        }
    }
}
