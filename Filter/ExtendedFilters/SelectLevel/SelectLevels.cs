﻿using LogAnalyzerLib.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Reader;
using System.Windows;
using LogAnalyzerLib;

namespace ExtendedFilters.SelectLevel
{
    /// <summary> Filter nach bestimmten Leveln. Zeige Auswahlmaske für die Levelauswahl
    /// </summary>
    class SelectLevels:Filter
    {
        public SelectLevels()
            : base("Select Levels", "Filter for multiple Levels",false)
        {
        }
        /// <summary> Zeige Auswahlmaske
        /// </summary>
        /// <param name="apply"></param>
        /// <returns></returns>
        public override object configure(List<LogEntry> logsfile, out bool apply)
        {
            SelectLevelMask selection = new SelectLevelMask();
            selection.ShowDialog();
            apply = selection.apply;
            return selection.selected;
        }
        /// <summary> Filter nach Leveln
        /// </summary>
        /// <param name="logsfile">Logeinträge</param>
        /// <param name="source">Nicht wichtig</param>
        /// <param name="additional">Dicitionary<string,bool> der Level</param>
        /// <returns></returns>

        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            var filters = additional as Dictionary<string, bool>;
            var filtered = logsfile.Where(l => filter(l, filters)).ToList();

            return filtered;
        }
        private bool filter(LogEntry entry, Dictionary<string, bool> filter)
        {
            string level = entry.level.ToLower();
            bool b = filter[level];
            return b;
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            List<string> rules = new List<string>();
            foreach (var kvp in (Dictionary<string, bool>)additional)
                if (kvp.Value)
                    rules.Add(kvp.Key);

            return "Filter for Levels (" + string.Join(",", rules) + ")";
        }
    }
}
