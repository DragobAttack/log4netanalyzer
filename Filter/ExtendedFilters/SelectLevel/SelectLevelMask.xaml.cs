﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LogAnalyzerLib;
using log4net;

namespace ExtendedFilters
{
    /// <summary>
    /// Interaktionslogik für SelectLevel.xaml
    /// </summary>
    public partial class SelectLevelMask : Window
    {
        public Dictionary<string, bool> selected = new Dictionary<string, bool>();
        public bool apply = false;

        public SelectLevelMask()
        {            
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            foreach(Log4Levels level in (Log4Levels[]) Enum.GetValues(typeof(Log4Levels)))
            {
                selected.Add(level.ToString().ToLower(), true);

                CheckBox cb = new CheckBox();
                cb.Content = level;
                cb.Click += Cb_Click;
                cb.IsChecked = true;
                cb.Margin = new Thickness(1);
                lvLevels.Items.Add(cb);
            }
        }

        private void Cb_Click(object sender, RoutedEventArgs e)
        {
            CheckBox send = (CheckBox)sender;
            Log4Levels level = (Log4Levels)Enum.Parse(typeof(Log4Levels), send.Content.ToString());
            selected[level.ToString().ToLower()] = (send.IsChecked.HasValue)?send.IsChecked.Value:false;
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            apply = true;
            this.Close();
        }
    }
}
