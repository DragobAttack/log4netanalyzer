﻿using LogAnalyzerLib.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Reader;
using FilterUtilities;

namespace ExtendedFilters.TimeSpan
{
    class TimeSpanFilter:Filter
    {
        public TimeSpanFilter()
            : base("Time Span Filter", "Filters for Log Entries created in a certain Timespan",false)
        {
        }

        public override object configure(List<LogEntry> logsfile, out bool apply)
        {
            DateTime start = DateTime.MinValue;
            DateTime end = DateTime.MaxValue;
            if (logsfile.Count() > 0)
            {
                start = logsfile.Min(m => m.timestamp);
                end = logsfile.Max(m => m.timestamp);
            }

            SelectTimeSpanMask mask = new SelectTimeSpanMask(start, end);

            mask.ShowDialog();

            DateTime[] times = new DateTime[] {
                mask.StartTime,
                mask.EndTime
            };

            apply = mask.ApplyValue;
            return times;
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            DateTime[] times = additional as DateTime[];

            return "Created between " + times.First().ToString() + " and " + times.Last().ToString();
        }
        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            List<LogEntry> found = new List<LogEntry>();

            DateTime[] times = additional as DateTime[];
            DateTime start = times.First();
            DateTime end = times.Last();

            foreach (var e in found)
                if (e.timestamp >= start && e.timestamp <= end)
                    found.Add(e);

            return found;
        }
    }
}
