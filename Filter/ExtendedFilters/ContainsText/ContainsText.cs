﻿using LogAnalyzerLib.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Reader;

namespace ExtendedFilters.ContainsText
{
    class ContainsTextFilter:Filter
    {
        public ContainsTextFilter()
            : base("Contains Text","Log Message contains text",false)
        {
        }

        public override object configure(List<LogEntry> logsfile, out bool apply)
        {
            ContainsTextMask mask = new ContainsTextMask();
            mask.ShowDialog();

            if (!string.IsNullOrEmpty(mask.searchString))
                apply = true;
            else
                apply = false;

            return mask.searchString;
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            string des = additional.ToString();

            if (des.Length > 12)
                des = additional.ToString() + "...";

            return "contains \"" + des + "\"";
        }

        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            logsfile.RemoveAll(e => !e.message.Contains(additional.ToString()));
            return logsfile;
        }
    }
}
