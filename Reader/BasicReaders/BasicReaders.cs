﻿using PluginSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicReaders
{
    /// <summary> Stelle Reader zum einlesen von Log Datein zur Verfügung
    /// </summary>
    public class BasicReaders:PluginToolbox
    {
        public BasicReaders():base("Basic Readers","",
            new XMLReader(),
            new XMLSpanReader())
        {
        }
    }
}
