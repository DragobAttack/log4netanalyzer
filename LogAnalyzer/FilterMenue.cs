﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzer
{
    class FilterMenue
    {
        List<FilterMenueItem> filter = new List<FilterMenueItem>();

        public void Add(FilterMenueItem item)
        {
            filter.Add(item);
        }

        public void updateActive(List<LogEntry> logsfile, LogEntry selected)
        {
            foreach (FilterMenueItem item in filter)
                item.udpateActive(logsfile,selected);
        }
    }
}
