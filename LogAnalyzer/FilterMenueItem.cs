﻿using LogAnalyzerLib.Filter;
using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LogAnalyzer
{
    class FilterMenueItem:MenuItem
    {
        public event EventHandler<Filter> filterSelected;

        public readonly Filter bound;

        public FilterMenueItem(Filter binding) :base()
        {
            this.bound = binding;
            this.Header = bound.name;
            this.ToolTip = bound.description;
        }

        protected override void OnClick()
        {
            filterSelected?.Invoke(this, bound);
            base.OnClick();
        }

        public void udpateActive(List<LogEntry> logsfile, LogEntry selected)
        {
            this.IsEnabled = bound.canExecuteOnEntry(logsfile,selected);
        }
    }
}
