﻿using LogAnalyzerLib.Filter;
using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace LogAnalyzer
{
    class FilterTreeItem:TreeViewItem
    {
        public event EventHandler<Filter> filterSelected;

        public readonly Filter bound;

        public FilterTreeItem(Filter binding) : base()
        {
            this.bound = binding;
            this.Header = bound.name;
            this.ToolTip = bound.description;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            filterSelected?.Invoke(this, bound);
            base.OnMouseLeftButtonDown(e);
        }


        public void udpateActive(List<LogEntry> logsfile, LogEntry selected)
        {
            this.IsEnabled = bound.canExecuteOnEntry(logsfile, selected);
        }
    }
}
