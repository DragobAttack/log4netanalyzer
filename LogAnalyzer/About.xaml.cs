﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LogAnalyzer
{
    /// <summary>
    /// Interaktionslogik für About.xaml
    /// </summary>
    public partial class About : Window
    {
        List<LoadedDLLs> dlls = new List<LoadedDLLs>();

        /// <summary> Erzeuge Aboutmaske mit DLL und Plugin Übersicht
        /// </summary>
        /// <param name="dllPaths"></param>
        public About(string[] dllPaths)
        {
            InitializeComponent();
            //Lese Versionsnummer aus
            lbVersion.Content = Assembly.GetEntryAssembly().GetName().Version.ToString();

            //gehe alle Datein durch
            foreach(string s in dllPaths)
            {
                //Lade Informationen über Datei
                LoadedDLLs dll = new LoadedDLLs(s);
                dlls.Add(dll);
            }
            //Fülle Liste
            dgLibs.ItemsSource = dlls;
        }
    }

    /// <summary> Informationen über eine geladene Bibliothek
    /// </summary>
    class LoadedDLLs
    {
        /// <summary>Name des Assembly in der DLL</summary>
        public string Name { get; private set; }
        /// <summary>Version der Assembly in der DLL</summary>
        public string Version { get; private set; }
        /// <summary>Wann wurde die Datei erstellt</summary>
        public DateTime Build_Time { get; private set; }

        public LoadedDLLs(string dll)
        {
            string path = System.IO.Path.GetFullPath(dll);
            
            var assembly = Assembly.LoadFile(path);

            Name = assembly.GetName().Name;
            Version = assembly.GetName().Version.ToString();

            Build_Time = File.GetLastWriteTime(dll);
        }
    }
}
