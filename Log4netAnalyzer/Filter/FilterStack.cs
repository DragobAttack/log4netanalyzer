﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using PluginSystem.Messages;

namespace LogAnalyzerLib.Filter
{
    /// <summary> Verwaltet eine Sammlung von hierarchischen Filtern
    /// </summary>
    public class FilterStack
    {
        /// <summary> Alle Filter mit Konfiguration
        /// </summary>
        List<StackEntry> _entries = new List<StackEntry>();

        /// <summary> Die Filter im Stack wurden geändert
        /// </summary>
        public event EventHandler stackChanged;

        /// <summary> Ein Filtervorgang wurde abgeschlossen
        /// </summary>
        public event EventHandler<int> finished;

        public readonly MessageSender messages;

        /// <summary> Beschreibungen welche Filter mit welcher Konfiguration auf dem Stack sind
        /// </summary>
        public string[] getView(List<LogEntry> entries)
        {
            string[] view = new string[_entries.Count];
            for (int i = 0; i < _entries.Count; i++)
                view[i] = _entries[i].filter.getDescriptionForCase(entries, _entries[i].parameter, _entries[i].configuration);

            return view;
        }

        /// <summary> Anzahl der Filter im Stapel
        /// </summary>
        public int Count
        {
            get
            {
                return _entries.Count;
            }
        }

        public FilterStack()
        {
            messages = new MessageSender(this.GetType().Name);
        }

        private void Filter_send_message(object sender, PluginMessage e)
        {
            messages.Send(e);
        }

        /// <summary> Wende alle Filter der Reihe nach auf Log Einträge an
        /// </summary>
        /// <param name="baseList"></param>
        /// <returns></returns>
        public List<LogEntry> filter(List<LogEntry> baseList)
        {
            messages.Clear();

            if (baseList == null)
            {
                finished?.Invoke(this, 0);
                return null;
            }

            for (int i = 0; i < _entries.Count; i++)
            {
                baseList = _entries[i].applyFilter(baseList);
            }

            finished?.Invoke(this, baseList.Count);

            return baseList;
        }

        /// <summary> Füge Filter zum Stack hinzu
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="entry"></param>
        /// <param name="config"></param>
        public void Push(Filter filter, LogEntry entry, object config)
        {
            filter.send_message += Filter_send_message;
            _entries.Add(new StackEntry(filter, entry, config));
            stackChanged?.Invoke(this,null);
        }

        /// <summary> Entferne den obersten Filter vom Stack
        /// </summary>
        public void Pop()
        {
            if (_entries.Count > 0)
            {
                _entries.RemoveAt(_entries.Count - 1);
                stackChanged?.Invoke(this, null);
            }
        }
        /// <summary> Leere den Stack
        /// </summary>
        public void Clear()
        {
            if (_entries.Count > 0)
            {
                _entries.Clear();
                stackChanged?.Invoke(this, null);
            }
        }
    }

    /// <summary> Filter auf dem Filter Stack mit Konfiguration
    /// </summary>
    class StackEntry
    {
        public readonly Filter filter;
        public readonly LogEntry parameter;
        public readonly object configuration;

        public StackEntry(Filter filter, LogEntry parameter, object configuration)
        {
            this.filter = filter;
            this.parameter = parameter;
            this.configuration = configuration;
        }

        /// <summary> Wende den Filter mit der Configuration an
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<LogEntry> applyFilter(List<LogEntry> list)
        {
            return filter.filter(list, parameter, configuration);
        }
    }
}
