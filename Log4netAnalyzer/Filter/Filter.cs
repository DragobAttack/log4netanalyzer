﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginSystem;
using PluginSystem.Messages;

namespace LogAnalyzerLib.Filter
{
    /// <summary> Basisklasse für LogFilter
    /// </summary>
    public abstract class Filter:PluginComponente
    {
        /// <summary> Wird ein Quelleintrag benötigt
        /// </summary>
        public readonly bool needsSource;

        /// <summary> Erzeuge Filterbasis
        /// </summary>
        /// <param name="name">Namde des Filters</param>
        /// <param name="description">Berschreibung des Filters</param>
        /// <param name="needsSource">Wird eine Quelleintrag benötigt</param>
        public Filter(string name, string description, bool needsSource):base(name,description)
        {
            this.needsSource = needsSource;
        }

        /// <summary> Wenn nötig, zeige dem Nutzer zusätzliche Auswahl/Konfigurationsmöglichkeiten
        /// </summary>
        /// <param name="apply"></param>
        /// <returns></returns>
        public virtual object configure(List<LogEntry> logsfile, out bool apply)
        {
            apply = true;
            return null;
        }

        /// <summary> Wende den Filter mit der übergebenen Konfiguration, auf die Übergebenen Logeinträge an
        /// </summary>
        /// <param name="logsfile">Logeinträge zum Filtern</param>
        /// <param name="source">Quelleintrag</param>
        /// <param name="additional">Zusätzliche Parameter für den Filter</param>
        /// <param name="message">Nachrichten aus dem Filter</param>
        /// <returns></returns>
        public List<LogEntry> filter(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            List<LogEntry> results = null;

            if (logsfile != null && logsfile.Count() > 0)
            {
                this.message(PluginMessage.MessageLevel.Info, "Apply (" + getDescriptionForCase(logsfile, source, additional) + ")");
                results = apply(logsfile, source, additional);
            }
            else
                this.message(PluginMessage.MessageLevel.Warning, "No log Entries");

            return results;
        }


        protected virtual List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            throw new Exception("Filter Basisklasse sollte nicht verwendet werden");
        }

        /// <summary> Erhalte Beschreibung für diesen Filter, mit der Übergebenen Konfiguration
        /// </summary>
        /// <param name="source"></param>
        /// <param name="additional"></param>
        /// <returns></returns>
        public virtual string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            return "apply fiter " + name;
        }

        public virtual bool canExecuteOnEntry(List<LogEntry> logsfile, LogEntry source)
        {
            if (needsSource)
                if (source == null)
                    return false;
            return true;
        }
    }
}
