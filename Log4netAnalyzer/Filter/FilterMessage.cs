﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Filter
{
    /// <summary> Meldung aus einem Filter
    /// </summary>
    public class PluginMessage
    {
        public enum MessageLevel { Info, Warning, Error };

        public MessageLevel Level { get; private set; }
        public string Message { get; private set; }
        public Filter Source { get; private set; }

        public PluginMessage(MessageLevel level, string message, Filter source)
        {
            this.Level = level;
            this.Message = message;
            this.Source = source;
        }
    }
}
