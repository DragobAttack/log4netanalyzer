﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;
using log4net.Filter;
using LogAnalyzerLib.Filter;
using System.Threading;
using PluginSystem.Messages;

namespace LogAnalyzerLib
{
    /// <summary>
    /// Steuerelment zum Anzeigen ausgelesener Logfileeinträge
    /// </summary>
    public partial class LogDisplay : UserControl
    {
        private CancellationTokenSource cancelRebuild = new CancellationTokenSource();
        /// <summary> Logfileeinträge die Angezeigt werden sollen
        /// </summary>
        private ObservableCollection<LogEntry> _source;

        /// <summary>
        /// </summary>
        private ObservableCollection<LogEntry> filtered;
        private long filteredCount = -1;

        public readonly FilterStack filter = new FilterStack();

        public readonly MessageSender statusMessages = new MessageSender("Log Display");

        public event EventHandler<LogEntry> selectedLogEntryChanged;
        public event EventHandler filterStackChanged;

        /// <summary> Setze Logfileeinträge zum Anzeigen
        /// </summary>
        public List<LogEntry> sourceList
        {
            set
            {
                try
                {
                    //Setzte Source
                    observableCollection = new ObservableCollection<LogEntry>(value);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            get
            {
                //Ist keine Source gesetzt
                if (_source == null)
                    return new List<LogEntry>();//Gebe leere Liste zurück
                else
                    return _source.ToList();//Gebe Source als Liste zurück
            }
        }
        /// <summary> Setze Logfileeinträge zum Anzeigen
        /// </summary>
        public ObservableCollection<LogEntry> observableCollection
        {
            set
            {
                if (_source != null)
                    _source.CollectionChanged -= sourceChanged;
                _source = value;
                filtered = _source;
                _source.CollectionChanged += sourceChanged;

                rebuild();
            }
        }

        /// <summary> Anzahl der Logeinträge, die eingelesen wurden
        /// </summary>
        public int Count
        {
            get
            {
                if (_source != null)
                    return _source.Count;
                else
                    return 0;
            }
        }
        /// <summary> Anzahl der Logeinträge, die nach dem Filtern angezeigt werden
        /// </summary>
        public int FilteredCount
        {
            get
            {
                //Wurde bereits ein Filtervorgang durchgeführt
                if (filtered != null)
                    return filtered.Count();//Gebe Anzahl der gefilterten Einträge zurück
                else if (_source != null)//Wurden überhaupt Logeinträge geladen
                    return _source.Count;//Gebe Gesamtzahl der Einträge zurück
                else
                    return 0;//0 als Standart Wert
            }
        }

        /// <summary> Index des Aktuell ausgewählten Eintrags
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return dgEntries.SelectedIndex;
            }
            set
            {
                dgEntries.ScrollIntoView(_source[value]);                
                dgEntries.CurrentItem = _source[value];
                dgEntries.SelectedIndex = value;
            }
        }

        /// <summary> Aktuell ausgewählter Eintrag
        /// </summary>
        public LogEntry SelectedItem
        {
            get
            {
                return dgEntries.SelectedItem as LogEntry;
            }
        }
        public LogDisplay()
        {
            InitializeComponent();

            filter.stackChanged += Filter_stackChanged;
            dgEntries.AutoGenerateColumns = false;            
        }

        /// <summary> Die Filter auf dem Filterstack haben sich geänedert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter_stackChanged(object sender, EventArgs e)
        {
            rebuild();
            resetView();
            filterStackChanged?.Invoke(this, e);
        }

        /// <summary> Wenn eine ObservableCollection als Source gesetzt ist und diese sich geändert hat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sourceChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Aktualisiere Anzeige
            rebuild();
        }
        /// <summary> Aktualsiere Anzeige, führe Filtervorgang neu aus
        /// </summary>
        /// <param name="resetCol"></param>
        private void rebuild(bool resetCol = true)
        {
            try
            {
                statusMessages.Clear();
                //Wenn bereits eine Aktualisierung läuft, breche sie ab
                cancelRebuild.Cancel();
                cancelRebuild = new CancellationTokenSource();
                CancellationToken token = cancelRebuild.Token;

                //Sollen Daten hineingeladen werden
                if (_source != null)
                {
                    //Speicher für die Anzeige Items
                    filtered = new ObservableCollection<LogEntry>();
                    this.dgEntries.ItemsSource = filtered;

                    //Lade Daten im Hintergrund ein
                    Task fill = Task.Run(() =>
                    {
                        //Wende Filter an
                        var newMessages = filter.filter(_source.ToList());
                        filteredCount = newMessages.Count;
                        //Informiere Nutzer über Ladefortschritt
                        UpdateableMessage loadingProgress = new UpdateableMessage(PluginMessage.MessageLevel.Info, "Loading Entries in View (0/" + newMessages.Count + ")", null);
                        statusMessages.Send(loadingProgress);

                        //Soll der Ladevorgang abgebrochen werden
                        if (token.IsCancellationRequested)
                        {
                            statusMessages.Send(new PluginMessage(PluginMessage.MessageLevel.Warning, "Loading in View was canceled", null));
                            //Lösche Nachricht
                            loadingProgress.delete();
                            return;
                        }

                        if (filteredCount > 0)
                        {
                            //Lade die Daten einzeln in die Anzeige
                            long currentIdex = 0;
                            foreach (var item in newMessages)
                            {
                                //Soll der Ladevorgang abgebrochen werden
                                if (token.IsCancellationRequested)
                                {
                                    //Lösche Nachricht
                                    loadingProgress.delete();
                                    return;
                                }

                                //Informiere Nutzer über Ladefortschritt
                                currentIdex++;
                                loadingProgress.updateMessage = "Loading Entries in View (" + currentIdex + "/" + newMessages.Count + ")";

                                //todo: kann beim Beenden zum Absturz führen

                                try
                                {
                                    //Lade einzelne LogMessage in die Anzeige
                                    if (!this.dgEntries.Dispatcher.HasShutdownStarted)
                                        this.dgEntries.Dispatcher.Invoke(() => filtered.Add(item), System.Windows.Threading.DispatcherPriority.Background);
                                    else
                                        return;
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (!this.dgEntries.Dispatcher.HasShutdownStarted)
                                this.dgEntries.Dispatcher.Invoke(() => filtered.Clear(), System.Windows.Threading.DispatcherPriority.Background);
                        }
                    });
                }
                else
                    this.dgEntries.ItemsSource = null;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        /// <summary> Erzwinge ein Neuanwenden der Filter und ein aktualisieren der Oberfläche
        /// </summary>
        public void update()
        {
            rebuild();
        }

        /// <summary> Lösche alle Suspicious Markierungen und Comments
        /// </summary>
        public void resetView()
        {
            if (_source != null)
            {
                foreach (LogEntry entry in _source)
                {
                    entry.suspicious = 0;
                    entry.comment = string.Empty;
                }
            }
        }

        /// <summary> Der Ausgewählte Logeintrag hat sich geändert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgEntries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Melde das
            selectedLogEntryChanged?.Invoke(this, dgEntries.SelectedItem as LogEntry);
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Delete:
                    filter.Pop();
                    break;

                case Key.PageDown:
                    e.Handled = true;
                    break;

                case Key.PageUp:
                    e.Handled = true;
                    break;
            }
        }
    }
}
