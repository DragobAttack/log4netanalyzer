﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins.Messages
{
    /// <summary> Meldung von einer Programm Komponente, deren Inhalt nachträglich geändert werden kann
    /// </summary>
    public class UpdateableMessage:PluginMessage
    {
        /// <summary> Der Inhalt hat sich geändert
        /// </summary>
        public event EventHandler updated;

        /// <summary> Die Meldung soll gelöscht werde
        /// </summary>
        private bool _delete = false;

        /// <summary> Soll die Meldung gelöscht werden
        /// </summary>
        public bool deleteRequested
        {
            get
            {
                return _delete;
            }
        }

        /// <summary> Ändere den Inhalt der Meldung und informiere darüber 
        /// </summary>
        public string updateMessage
        {
            set
            {
                base.Message = value;
                updated?.Invoke(this, null);
            }
        }

        /// <summary> Erzeuge neue änderbare Meldung
        /// </summary>
        /// <param name="level">Level der Meldung</param>
        /// <param name="startMessage">Nachricht zu Beginn</param>
        /// <param name="source">Absender</param>
        public UpdateableMessage(MessageLevel level, string startMessage, PluginComponente source):base(level,startMessage,source)
        {
        }

        /// <summary> Informiere, dass die Nachricht gelöscht werden soll</summary>
        public void delete()
        {
            //Die Nachricht soll gelöscht werden
            _delete = true;
            //Informiere alle darüber
            updated?.Invoke(this, null);
        }
    }
}
