﻿using LogAnalyzerLib.Plugin.Messages;
using LogAnalyzerLib.Plugins.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins
{
    public class PluginComponente
    {
        #region Basic Information
        /// <summary> Name des Filters
        /// </summary>
        public readonly string name;

        string _toolbox;

        public string toolbox
        {
            get
            {
                return _toolbox;
            }
            internal set
            {
                _toolbox = value;
            }
        }
        /// <summary> Genauere Beschreibung des Filters
        /// </summary>
        public readonly string description;

        public event EventHandler<PluginMessage> send_message;
        #endregion

        public PluginComponente(string name, string description)
        {
            this.name = name;
            this.description = description;
        }


        public override string ToString()
        {
            return _toolbox + "\\" + name;
        }

        protected void message(PluginMessage.MessageLevel level, string message)
        {
            PluginMessage m = new PluginMessage(level, message, this);
            send_message?.Invoke(this, m);
        }
        protected void message(PluginMessage message)
        {
            send_message?.Invoke(this, message);
        }
        protected UpdateableMessage updateableMessage(PluginMessage.MessageLevel level, string message)
        {
            UpdateableMessage upmess = new UpdateableMessage(level, message, this);
            send_message?.Invoke(this, upmess);
            return upmess;
        }
    }
}