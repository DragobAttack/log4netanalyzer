﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace LogAnalyzerLib.Plugins
{
    /// <summary> Loader für Filter Toolboxen aus DLLs
    /// </summary>
    public class ToolboxLoader
    {
        /// <summary> Speicherort für Toolbox Dateien
        /// </summary>
        private string _toolboxDirectory;
        /// <summary> Alle geladenen Toolboxen
        /// </summary>
        private List<PluginToolbox> _toolboxes = new List<PluginToolbox>();

        /// <summary> Zugriff auf die geladenen Toolboxen von außen
        /// </summary>
        public PluginToolbox[] toolboxes
        {
            get
            {
                return _toolboxes.ToArray();
            }
        }

        /// <summary> Erzeuge neuen ToolboxLoader
        /// </summary>
        /// <param name="toolboxDirectory">Standart speicherort der Toolbox DLLs</param>
        /// <param name="load">Lade die Toolboxen</param>
        public ToolboxLoader(string toolboxDirectory, bool load = false)
        {
            _toolboxDirectory = toolboxDirectory;
            if (load)
                this.load();
        }
        /// <summary> Lade Toolboxen aus dem Standart Verzeichnis
        /// </summary>
        public void load()
        {
            if(string.IsNullOrEmpty(_toolboxDirectory) || !Directory.Exists(_toolboxDirectory))
            {
                throw new DirectoryNotFoundException("Standart Speicherort nicht gesetzt.");
            }

            foreach (string file in Directory.GetFiles(_toolboxDirectory, "*.dll"))
                load(file);
        }
        /// <summary> Lade Toolboxen aus dem übergebenen Verzeichnis
        /// </summary>
        /// <param name="file"></param>
        public void load(string file)
        {
            string absPath = Path.GetFullPath(file);
            var content = Assembly.LoadFile(absPath);
            Type[] types = content.GetTypes();

            Type[] boxes = Array.FindAll(types, t => typeof(PluginToolbox).IsAssignableFrom(t));

            foreach(Type box in boxes)
            {
                var newBox = Activator.CreateInstance(box);
                _toolboxes.Add(newBox as PluginToolbox);
            }
        }

        public T[] getAll<T>()
        {
            List<T> allFound = new List<T>();

            foreach(var box in toolboxes)
            {
                foreach (var tool in box.getPlugins<T>())
                    allFound.Add(tool);
            }
            return allFound.ToArray();
        }
    }
}
