﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.Data;

namespace LogAnalyzerLib
{
    /// <summary>
    /// Interaktionslogik für LogEntryDisplay.xaml
    /// </summary>
    public partial class LogEntryDisplay : UserControl
    {
        private LogEntry _toDisplay;
        DataTable properties = new DataTable();

        public LogEntry toDisplay
        {
            set
            {
                _toDisplay = value;
                if (_toDisplay != null)
                    analyzeEntry(_toDisplay);
                else
                    generateTableRows();
            }
            get
            {
                return _toDisplay;
            }
        }

        public LogEntryDisplay()
        {
            properties.Columns.Add(new DataColumn("Property", typeof(string)));
            properties.Columns.Add(new DataColumn("Value", typeof(string)));

            properties.PrimaryKey = new DataColumn[] { properties.Columns["Property"] };

            InitializeComponent();

            generateTableRows();

            dgProperties.ItemsSource = properties.DefaultView;
        }

        private void generateTableRows()
        {
            properties.Clear();
            Type t = typeof(LogEntry);
            foreach(var p in t.GetProperties())
            {
                if (!LogEntry.hiddenProperties.Contains(p.Name))
                {
                    var row = properties.NewRow();
                    row.BeginEdit();
                    row.SetField(0, p.Name);
                    row.SetField(1, string.Empty);
                    row.EndEdit();

                    properties.Rows.Add(row);
                }
            }
        }

        private void analyzeEntry(LogEntry entry)
        {
            var type = entry.GetType();
            foreach(var p in type.GetProperties())
            {
                var row = properties.Select("Property = '" + p.Name + "'");
                if(row != null && row.Any())
                    row.First().SetField("Value", p.GetValue(entry));
            }
        }
    }
}
