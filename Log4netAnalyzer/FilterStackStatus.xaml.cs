﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LogAnalyzerLib.Plugins.Messages;

namespace LogAnalyzerLib
{
    /// <summary>
    /// Interaktionslogik für FilterStackStatus.xaml
    /// </summary>
    public partial class FilterStackStatus : UserControl
    {
        bool _showInfo;
        bool _showWarning;
        bool _showError;

        public event EventHandler<FilterStackStatusLevelSelectionArgs> LevelSelectionChanged;

        public bool showInfo
        {
            get { return _showInfo; }
            private set
            {
                _showInfo = value;
                sendUpdate();
            }
        }
        public bool showWarning
        {
            get { return _showWarning; }
            private set
            {
                _showWarning = value;
                sendUpdate();
            }
        }
        public bool showError
        {
            get { return _showError; }
            private set
            {
                _showError = value;
                sendUpdate();
            }
        }

        public bool anySelected
        {
            get
            {
                return showInfo || showWarning || showError;
            }
        }

        public int InfoCount
        {
            set
            {
                lbInfoCount.Dispatcher.Invoke(() => lbInfoCount.Content = value);
            }
        }
        public int WarningCount
        {
            set
            {
                lbWarnCount.Dispatcher.Invoke(() => lbWarnCount.Content = value);
            }
        }
        public int ErrorCount
        {
            set
            {
                lbErrorCount.Dispatcher.Invoke(() => lbErrorCount.Content = value);
            }
        }

        public FilterStackStatus()
        {
            InitializeComponent();
        }

        private void tgbInfo_Click(object sender, RoutedEventArgs e)
        {
            showInfo = getBoolValue(tgbInfo.IsChecked);
        }

        private void tgbWarn_Click(object sender, RoutedEventArgs e)
        {
            showWarning = getBoolValue(tgbWarn.IsChecked);
        }

        private void tgbError_Click(object sender, RoutedEventArgs e)
        {
            showError = getBoolValue(tgbError.IsChecked);
        }

        private bool getBoolValue(bool? b)
        {
            return b.HasValue ? b.Value : false;
        }

        private void sendUpdate()
        {
            List<MessageLevel> levels = new List<MessageLevel>();

            if (showInfo) levels.Add(MessageLevel.Info);
            if (showWarning) levels.Add(MessageLevel.Warning);
            if (showError) levels.Add(MessageLevel.Error);

            LevelSelectionChanged?.Invoke(this, new FilterStackStatusLevelSelectionArgs(levels.ToArray()));
        }
    }

    public class FilterStackStatusLevelSelectionArgs:EventArgs
    {
        public readonly MessageLevel[] levels;

        public FilterStackStatusLevelSelectionArgs(MessageLevel[] levels)
        {
            this.levels = levels;
        }
    }
}
