﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib
{
    public static class ArrayExtension
    {
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }

    public static class IEnumerableExtension
    {
        public static int[] FindAllIndexof<T>(this IEnumerable<T> values, Predicate<T> match)
        {
            return values.Select((value, index) => match(value) ? index : -1)
            .Where(index => index != -1).ToArray();
        }
    }
}
