﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LogAnalyzerLib.Reader
{
    [XmlRoot("log")]
    public class Log4NetXmlLogFile
    {
        [XmlElement(ElementName = "event")]
        public LogEntry[] entries { get; set; }
    }

    public class LogEntry
    {
        public static readonly string[] hiddenProperties = { "suspicious" };

        public long id { get; set; }

        [XmlAttribute("logger")]
        public string logger { get; set; }

        [XmlAttribute("timestamp")]
        public DateTime timestamp { get; set; }

        [XmlAttribute("level")]
        public string level { get; set; }

        [XmlAttribute("thread")]
        public int thread { get; set; }

        [XmlAttribute("domain")]
        public string domain { get; set; }

        [XmlAttribute("username")]
        public string username { get; set; }

        [XmlElement(ElementName = "message")]
        public string message { get; set; }

        public int suspicious { get; set; }
        public string comment { get; set; }
    }
}
