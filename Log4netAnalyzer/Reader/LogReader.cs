﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using System.Xml.Serialization;
using System.Xml;
using PluginSystem;
using PluginSystem.Messages;

namespace LogAnalyzerLib.Reader
{
    public abstract class LogReader:PluginComponente
    {
        protected List<LogEntry> _entries = new List<LogEntry>();

        protected bool _loaded = false;

        public event EventHandler logLoaded;

        public MessageSender messages;

        public bool loaded
        {
            get
            {
                return _loaded;
            }
            protected set
            {
                if (value)
                    logLoaded.Invoke(this, new EventArgs());
                this._loaded = value;
            }
        }

        public LogReader(string name, string description):base(name,description)
        {
            messages = new MessageSender("Log Reader");
            base.send_message += LogReader_send_message;
        }

        private void LogReader_send_message(object sender, PluginMessage e)
        {
            messages.Send(e);
        }

        public virtual bool canProcess(string parameter)
        {
            return false;
        }

        public virtual bool configure()
        {
            return true;
        }

        public virtual void read()
        {

        }

        public List<LogEntry> pullEntries()
        {
            var returnVal = new List<LogEntry>(_entries);
            _entries.Clear();

            return returnVal;
        }

        public List<LogEntry> peekEntries()
        {
            return new List<LogEntry>(_entries);
        }
    }
}
