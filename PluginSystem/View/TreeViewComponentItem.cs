﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PluginSystem.View
{
    class TreeViewComponentItem:TreeViewItem
    {
        public readonly PluginComponente bound;

        public TreeViewComponentItem(PluginComponente component):base()
        {
            this.Header = component.name;
            this.ToolTip = component.description;
            this.bound = component;
        }
    }
}
