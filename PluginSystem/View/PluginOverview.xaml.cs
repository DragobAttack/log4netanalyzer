﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PluginSystem.View
{
    /// <summary>
    /// Interaktionslogik für PluginOverview.xaml
    /// </summary>
    public partial class PluginOverview : UserControl
    {
        private ToolboxLoader _toDisplay;

        public ToolboxLoader toDisplay
        {
            get
            {
                return _toDisplay;
            }
            set
            {
                _toDisplay = value;
                updateView();
            }
        }

        public PluginOverview()
        {
            InitializeComponent();
        }

        public void updateView()
        {
            tvDisplay.Items.Clear();
            foreach(PluginToolbox box in _toDisplay.toolboxes)
            {
                TreeViewBoxItem bItem = new TreeViewBoxItem(box);
                bItem.MouseDown += Item_MouseDown;  
                bItem.Selected += Item_Selected;
                foreach (PluginComponente component in box.tools)
                {
                    TreeViewComponentItem cItem = new TreeViewComponentItem(component);
                    cItem.MouseDown += Item_MouseDown;
                    cItem.Selected += Item_Selected;
                    bItem.Items.Add(cItem);
                }
                tvDisplay.Items.Add(bItem);
            }
        }

        private void Item_Selected(object sender, RoutedEventArgs e)
        {
            updateSelection(sender);
        }

        private void Item_MouseDown(object sender, MouseButtonEventArgs e)
        {
            updateSelection(sender);
        }

        private void updateSelection(object sender)
        {
            if (sender is TreeViewBoxItem)
            {
                TreeViewBoxItem tvi = sender as TreeViewBoxItem;
                foreach(var child in tvi.Items)
                {
                    TreeViewComponentItem tvc = child as TreeViewComponentItem;
                    if (tvc.IsSelected)
                        return;
                }
                lbInfo.Content = tvi.bound.name + ": " + tvi.bound.description;
            }
            if (sender is TreeViewComponentItem)
            {
                TreeViewComponentItem tvc = sender as TreeViewComponentItem;
                lbInfo.Content = tvc.bound.name + ": " + tvc.bound.description;
            }
        }
    }
}
