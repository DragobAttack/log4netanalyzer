﻿using PluginSystem.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSystem
{
    public class PluginComponente
    {
        #region Basic Information
        /// <summary> Name der Komponente
        /// </summary>
        public readonly string name;

        /// <summary> Toolbox, zu dem diese Komponente gehört
        /// </summary>
        string _toolbox;
        public string toolbox
        {
            get
            {
                return _toolbox;
            }
            internal set
            {
                _toolbox = value;
            }
        }

        /// <summary> Genauere Beschreibung des Filters
        /// </summary>
        public readonly string description;

        /// <summary> Setzte Nachricht ab
        /// </summary>
        public event EventHandler<PluginMessage> send_message;
        #endregion

        public PluginComponente(string name, string description)
        {
            this.name = name;
            this.description = description;
        }


        public override string ToString()
        {
            return _toolbox + "\\" + name;
        }

        /// <summary> Setzte Nachricht ab
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        protected void message(PluginMessage.MessageLevel level, string message)
        {
            PluginMessage m = new PluginMessage(level, message, this);
            send_message?.Invoke(this, m);
        }
        /// <summary> Setzte Nachricht ab
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        protected void message(PluginMessage message)
        {
            send_message?.Invoke(this, message);
        }
        /// <summary> Setzte aktualisierbare Nachricht ab und gebe sie zum bearbeiten zurück
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        protected UpdateableMessage updateableMessage(PluginMessage.MessageLevel level, string message)
        {
            UpdateableMessage upmess = new UpdateableMessage(level, message, this);
            send_message?.Invoke(this, upmess);
            return upmess;
        }
    }
}
