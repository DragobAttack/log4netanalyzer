﻿using PluginSystem.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PluginSystem.Messages.Controls
{
    /// <summary>
    /// Interaktionslogik für FilterStackStatus.xaml
    /// </summary>
    public partial class MessageStackStatus : UserControl
    {
        bool _showInfo;
        bool _showWarning;
        bool _showError;

        public event EventHandler<FilterStackStatusLevelSelectionArgs> LevelSelectionChanged;

        private MessageReceiver _toDisplay;

        public MessageReceiver ToDisplay
        {
            get
            {
                return _toDisplay;
            }
            set
            {
                if (_toDisplay != null)
                    _toDisplay.messagesChanged -= _toDisplay_messagesChanged;
                _toDisplay = value;
                _toDisplay.messagesChanged += _toDisplay_messagesChanged;
            }
        }

        public bool showInfo
        {
            get { return _showInfo; }
            private set
            {
                _showInfo = value;
                tgbInfo.IsChecked = value;
                sendUpdate();
            }
        }
        public bool showWarning
        {
            get { return _showWarning; }
            private set
            {
                _showWarning = value;
                tgbWarn.IsChecked = value;
                sendUpdate();
            }
        }
        public bool showError
        {
            get { return _showError; }
            private set
            {
                _showError = value;
                tgbError.IsChecked = value;
                sendUpdate();
            }
        }

        public bool anySelected
        {
            get
            {
                return showInfo || showWarning || showError;
            }
        }

        public int InfoCount
        {
            set
            {
                lbInfoCount.Dispatcher.Invoke(() => lbInfoCount.Content = value);
            }
        }
        public int WarningCount
        {
            set
            {
                lbWarnCount.Dispatcher.Invoke(() => lbWarnCount.Content = value);
            }
        }
        public int ErrorCount
        {
            set
            {
                lbErrorCount.Dispatcher.Invoke(() => lbErrorCount.Content = value);
            }
        }

        public MessageStackStatus()
        {
            InitializeComponent();
        }

        private void _toDisplay_messagesChanged(object sender, EventArgs e)
        {
            InfoCount = _toDisplay.messagesCount(PluginMessage.MessageLevel.Info);
            WarningCount = _toDisplay.messagesCount(PluginMessage.MessageLevel.Warning);
            ErrorCount = _toDisplay.messagesCount(PluginMessage.MessageLevel.Error);
        }

        private void tgbInfo_Click(object sender, RoutedEventArgs e)
        {
            showInfo = getBoolValue(tgbInfo.IsChecked);
        }

        private void tgbWarn_Click(object sender, RoutedEventArgs e)
        {
            showWarning = getBoolValue(tgbWarn.IsChecked);
        }

        private void tgbError_Click(object sender, RoutedEventArgs e)
        {
            showError = getBoolValue(tgbError.IsChecked);
        }

        private bool getBoolValue(bool? b)
        {
            return b.HasValue ? b.Value : false;
        }

        private void sendUpdate()
        {
            List<PluginMessage.MessageLevel> levels = new List<PluginMessage.MessageLevel>();

            if (showInfo) levels.Add(PluginMessage.MessageLevel.Info);
            if (showWarning) levels.Add(PluginMessage.MessageLevel.Warning);
            if (showError) levels.Add(PluginMessage.MessageLevel.Error);

            LevelSelectionChanged?.Invoke(this, new FilterStackStatusLevelSelectionArgs(levels.ToArray()));
        }
    }

    public class FilterStackStatusLevelSelectionArgs:EventArgs
    {
        public readonly PluginMessage.MessageLevel[] levels;

        public FilterStackStatusLevelSelectionArgs(PluginMessage.MessageLevel[] levels)
        {
            this.levels = levels;
        }
    }
}
