﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSystem.Messages
{
    //Sende PluginMessages
    public class MessageSender
    {
        /// <summary> Der Sender setzt eine Nachricht ab </summary>
        public event EventHandler<PluginMessage> send;
        /// <summary> Lösche alle Einträge von einer Komponente </summary>
        public event EventHandler<PluginComponente> clearComponent;
        /// <summary> Lösche alle Einträge von diesem Sender </summary>
        public event EventHandler clearSender;

        /// <summary> Name dieses Senders
        /// </summary>
        private readonly string _senderName;
        public string SenderName
        {
            get
            {
                return _senderName;
            }
        }

        /// <summary> Erzeuge neuen Sender
        /// </summary>
        /// <param name="name">Name des Senders</param>
        public MessageSender(string name)
        {
            this._senderName = name;
        }

        /// <summary> Sende Nachricht an alle Empfänger
        /// </summary>
        /// <param name="message"></param>
        public void Send(PluginMessage message)
        {
            message.Sender = this;
            send?.Invoke(this, message);
        }

        /// <summary> Lösche alle Nachrichten von diesem Sender
        /// </summary>
        public void Clear()
        {
            clearSender?.Invoke(this, null);
        }
        /// <summary> Lösche alle Nachrichten von der übergebenen Komponente
        /// </summary>
        /// <param name="component"></param>
        public void Clear(PluginComponente component)
        {
            clearComponent?.Invoke(this, component);
        }

        public override string ToString()
        {
            return _senderName;
        }
    }
}
