﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSystem.Messages
{
    /// <summary> Meldung von einer Programm Komponente
    /// </summary>
    public class PluginMessage
    {
        public enum MessageLevel { Info, Warning, Error };

        public DateTime TimeStamp { get; }
        public MessageLevel Level { get; private set; }
        public virtual string Message { get; protected set; }
        public PluginComponente Component { get; private set; }
        public MessageSender Sender { get; internal set; }

        public PluginMessage(MessageLevel level, string message, PluginComponente component)
        {
            this.TimeStamp = DateTime.Now;
            this.Level = level;
            this.Message = message;
            this.Component = component;
        }
    }
}
