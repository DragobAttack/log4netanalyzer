﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSystem.Messages
{
    /// <summary> Empfange PluginMessages von MessageSendern
    /// </summary>
    public class MessageReceiver
    {
        /// <summary> Alle Empfangenen Nachrichten
        /// </summary>
        private List<PluginMessage> _messages = new List<PluginMessage>();
        /// <summary> Die Nachrichten Sammlung hat sich geändert
        /// </summary>
        public event EventHandler messagesChanged;    
        /// <summary> Zugriff auf die Nachrichten Sammlung
        /// </summary>
        public PluginMessage[] messages
        {
            get
            {
                return _messages.ToArray();
            }
        }

        /// <summary> Lausche ab sofort auf Nachrichten von diesem Sender
        /// </summary>
        /// <param name="sender"></param>
        public void AddSender(MessageSender sender)
        {
            sender.send += Sender_send;
            sender.clearSender += Sender_clearSender;
            sender.clearComponent += Sender_clearComponent;
        }
        /// <summary> Lausche nicht mehr auf Nachrichten von diesem Sender
        /// </summary>
        /// <param name="sender"></param>
        public void RemoveSender(MessageSender sender)
        {
            sender.send -= Sender_send;
            sender.clearSender -= Sender_clearSender;
            sender.clearComponent -= Sender_clearComponent;
        }
        /// <summary> Meldung von einem Sender, alle Nachrichten einer Komponenten zu löschen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sender_clearComponent(object sender, PluginComponente e)
        {
            //Finde alle UpdateableMessages der Komponente
            foreach (var m in _messages.FindAll(m => m.Component == e && m is UpdateableMessage))
            {
                //Deaboniere Update Event
                var upmess = m as UpdateableMessage;
                upmess.updated -= Upmess_updated;
            }

            //Lösche alle Nachrichten dieser Komponente
            _messages.RemoveAll(m => m.Component == e);
            //Die Nachrichten haben sich geändert
            messagesChanged?.Invoke(this, null);
        }

        /// <summary> Lösche alle Nachrichten von einem Sender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sender_clearSender(object sender, EventArgs e)
        {
            //Suche alle Nachrichten von diesem Sender
            foreach (var m in _messages.FindAll(m => m.Sender == sender))
            {
                if(m is UpdateableMessage)
                {
                    //Trenne Update Event
                    var upmess = m as UpdateableMessage;
                    upmess.updated -= Upmess_updated;
                }
            }
            //Lösche alle Nachrichten von diesem Sender
            _messages.RemoveAll(m => m.Sender == sender);
            //Die Nachrichten haben sich geändert
            messagesChanged?.Invoke(this, null);
        }

        /// <summary> Ein Message Sender hat eine Nachricht gesendet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sender_send(object sender, PluginMessage e)
        {
            //Verhindere Duplikate
            if (_messages.Any() && _messages.Last() == e)
                return;

            //Speichere Message
            _messages.Add(e);

            //Ist es eine Updateable Message
            if(e is UpdateableMessage)
            {
                //Abboniere update Event
                var upmess = e as UpdateableMessage;
                upmess.updated += Upmess_updated;                
            }
            //Die Nachrichten haben sich geändert
            messagesChanged?.Invoke(this, null);
        }

        /// <summary> Eine Updateable Message hat sich geändert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Upmess_updated(object sender, EventArgs e)
        {
            //Ist der Sender eine Updateble Message (sollte immer sein)
            if(sender is UpdateableMessage)
            {                
                UpdateableMessage upmes = sender as UpdateableMessage;
                //Soll die Nachricht gelöscht werden
                if (upmes.deleteRequested)
                    _messages.Remove(upmes);//Lösche die Nachricht
            }
            //Die Nachrichten haben sich geändert
            messagesChanged?.Invoke(this, null);
        }
        /// <summary> Anzahl der gespeicherten Nachrichten
        /// </summary>
        /// <returns></returns>
        public int messagesCount()
        {
            return _messages.Count;
        }
        /// <summary> Anzahl der gespeicherten Nachrichten für einen MessageLevel
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public int messagesCount(PluginMessage.MessageLevel level)
        {
            return _messages.Count(m => m.Level == level);
        }
        /// <summary> Alle Nachrichten für die gewählten Level
        /// </summary>
        /// <param name="levels"></param>
        /// <returns></returns>
        public PluginMessage[] getMessages(PluginMessage.MessageLevel[] levels)
        {
            return messages.Where(m => contains(levels,m.Level)).ToArray();
        }

        /// <summary> Enthält ein Array ein Element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        private bool contains<T>(T[] array, T element)
        {
            foreach (T i in array)
                if (i.Equals(element))
                    return true;
            return false;
        }
    }
}
